﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;

namespace HomeWork_PageRank
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //put your site link bellow
            var pg = new PageRank(@"https://vc.ru/", "vc.ru/", 100);
            pg.Start();

            Console.ReadLine();
        }
    }

    public class Rank
    {
        public int Id { get; set; }
        public double Value { get; set; }
    }
    public class PageRank
    {
        #region Private Members

        private readonly string _siteUrl;
        private readonly string _siteMask;
        private int _matrixSize;

        private readonly Dictionary<string, int> _urlsDictionary;
        private readonly List<string> _urls;

        #endregion

   
        // Construction class
        public PageRank(string siteUrl, string siteMask, int matrixSize)
        {
            _siteUrl = siteUrl;
            _siteMask = siteMask;
            _matrixSize = matrixSize;

            _urlsDictionary = new Dictionary<string, int>();
            _urls = new List<string>();
        }
        
  
        // Default constructor
       
        public PageRank() : this(string.Empty, string.Empty, 0)
        {

        }

        // Page rank calculation process
        public async void Start()
        {
            await CollectUrlsAsync();
            var matrix = await BuildMatrixAsync();
            await CalculatePageRankAsync(matrix, _matrixSize, GetMatrixLinksCount(matrix));

            SparseMatrixPageRank(matrix, _matrixSize, GetMatrixLinksCount(matrix));
            SparseMatrixPageRankParallel(matrix, _matrixSize, GetMatrixLinksCount(matrix));
        }

        //Links collector
        
        private async Task CollectUrlsAsync()
        {
            Console.WriteLine("** Collecting links form site **");

            _urls.Clear();
            _urlsDictionary.Clear();

            Console.WriteLine($"[0]: {_siteUrl}");

            _urlsDictionary.Add(_siteUrl, 0);
            _urls.Add(_siteUrl);

            int i = 1, j = 0;

            object obj = new object();

            while (_urls.Count < _matrixSize)
            {
                var webSource =
                await BrowsingContext
                    .New(Configuration.Default.WithDefaultLoader())
                    .OpenAsync($"{_urls[j++]}");
                

                var urls =
                    webSource.QuerySelectorAll("a")
                        .Select(x => x.GetAttribute("href"))
                        .Where(y => !string.IsNullOrEmpty(y) && y.Contains(_siteMask));

                foreach (var url in urls)
                {
                    if (!_urlsDictionary.ContainsKey(url) && i < _matrixSize)
                    {
                        Console.WriteLine($"[{i}]: {url}");

                        _urlsDictionary.Add(url, i++);
                        _urls.Add(url);
                    }
                }
            }

            Console.WriteLine("** Done **");
            Console.WriteLine();
        }

        /// <summary>
        /// Build adjacency matrix by collected links
        /// </summary>
        /// <returns>Adjacency matrix of collected links</returns>
        private async Task<int[,]> BuildMatrixAsync()
        {
            Console.WriteLine("** Build  adjacency matrix **");

            int[,] matrix = new int[_matrixSize, _matrixSize];

            var watch = new Stopwatch();
            watch.Start();

            int i = 0;
            foreach (var url in _urls)
            {
                var urlSource =
                    await BrowsingContext
                        .New(Configuration.Default.WithDefaultLoader())
                        .OpenAsync($"{url}");

                var hrefs = urlSource.QuerySelectorAll("a")
                    .Select(x => x.GetAttribute("href"))
                    .Where(x => !string.IsNullOrEmpty(x) && x.Contains(_siteMask));

                foreach (var href in hrefs)
                    if (_urlsDictionary.ContainsKey(href) && i != _urlsDictionary[href])
                        matrix[i, _urlsDictionary[href]] = 1;

                i++;
            }
            
            watch.Stop();

            using (var writer = new StreamWriter("matrix.txt", true))
            {
                for (int h = 0; h < _urlsDictionary.Count; h++)
                {
                    for (int j = 0; j < _urlsDictionary.Count; j++)
                    {
                        writer.WriteLine(matrix[h, j] + " ");
                        Console.Write(matrix[h, j] + " ");
                    }

                    writer.WriteLine();
                    Console.WriteLine();
                }
            }

            Console.WriteLine($"Matrix built, elapsed: {watch.Elapsed.TotalSeconds} (s)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            return matrix;
        }

        //Link sum calculation
        private int[] GetMatrixLinksCount(int[,] martrix)
        {
            var linksCount = new int[_matrixSize];

            for (int i = 0; i < _matrixSize; i++)
            {
                int linkCount = 0;

                for (int j = 0; j < _matrixSize; j++)
                    linkCount += martrix[i, j];

                linksCount[i] = linkCount;
            }

            return linksCount;
        }

 
        /// PageRank calculation process
  
   
        private Task<IEnumerable<double>> CalculatePageRankAsync(int[,] matrix, int matrixSize, int[] linksSum)
        {
            Console.WriteLine("** PageRank caluculation **");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var pagesRankOld = new double[matrixSize];
            var pagesRank = new double[matrixSize];

            for (int i = 0; i < matrixSize; i++)
                pagesRankOld[i] = 1.0D / matrixSize;

            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    pagesRank[j] = 1 - 0.85;

                    var sum = 0.0D;

                    for (int h = 0; h < matrixSize; h++)
                    {
                        if (matrix[j, h] != 0 && linksSum[h] != 0)
                        {
                            sum += pagesRankOld[h] / linksSum[h];
                        }

                    }

                    sum = 0.85 * sum;
                    pagesRank[j] += sum;
                }

                pagesRankOld = pagesRank;
            }

            stopwatch.Stop();

            var sortablePageRank = new List<Rank>();
            for (int i = 0; i < matrixSize; i++)
                sortablePageRank.Add(new Rank() { Id = i, Value = pagesRank[i] });

            foreach (var rank in sortablePageRank.OrderByDescending(x => x.Value))
            {
                Console.WriteLine($"[{rank.Id}]: {rank.Value}");
            }

            Console.WriteLine($"Calculated, elapsed: {stopwatch.Elapsed.TotalMilliseconds}(ms)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            return Task.FromResult(pagesRank.AsEnumerable());
        }
        
        /// Sparse matrix calculation
      
        private void SparseMatrixPageRank(int[,] matrix, int matrixSize, int[] linksSum)
        {
            Console.WriteLine("** SparseMatrixPageRank caluculation **");
            Console.WriteLine();

            #region SparseMatrix

            Console.WriteLine("** SparseMatrix building **");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var nnz = new List<int>();

            var nnzCount = 0;
            var nnzCountList = new List<int> { nnzCount };

            var nnzPosition = new List<int>();

            for (int i = 0; i < _matrixSize; i++)
            {
                var localNnzCount = 0;

                for (int j = 0; j < _matrixSize; j++)
                {
                    if (matrix[i, j] != 0)
                    {
                        localNnzCount++;
                        nnz.Add(matrix[i, j]);
                        nnzPosition.Add(j);
                    }
                }

                nnzCount += localNnzCount;
                nnzCountList.Add(nnzCount);
            }

            stopwatch.Stop();

            Console.WriteLine($"Built completed, elapsed: {stopwatch.Elapsed.TotalMilliseconds}(ms)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            #endregion

            #region PageRank

            Console.WriteLine("** PageRank caluculation **");

            stopwatch = new Stopwatch();
            stopwatch.Start();

            var pagesRankOld = new double[matrixSize];
            var pagesRank = new double[matrixSize];

            for (int i = 0; i < matrixSize; i++)
                pagesRankOld[i] = 1.0D / matrixSize;

            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    pagesRank[j] = 1 - 0.85;

                    var sum = 0.0D;

                    for (int h = 0; h < matrixSize; h++)
                    {
                        if (GetSparseMatrixValue(j, h, nnz, nnzCountList, nnzPosition) > -1 && linksSum[h] != 0)
                        {
                            sum += pagesRankOld[h] / linksSum[h];
                        }
                    }

                    sum = 0.85 * sum;
                    pagesRank[j] += sum;
                }

                pagesRankOld = pagesRank;
            }

            stopwatch.Stop();

            var sortablePageRank = new List<Rank>();
            for (int i = 0; i < matrixSize; i++)
                sortablePageRank.Add(new Rank() { Id = i, Value = pagesRank[i] });

            foreach (var rank in sortablePageRank.OrderByDescending(x => x.Value))
            {
                Console.WriteLine($"[{rank.Id}]: {rank.Value}");
            }

            Console.WriteLine($"Calculated, elapsed: {stopwatch.Elapsed.TotalMilliseconds}(ms)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            #endregion

            Console.WriteLine("** Done SparseMatrixPageRank caluculation **");
            Console.WriteLine();
        }

     
        // Sparse matrix parallel calculation using Parallel.For
        
        private void SparseMatrixPageRankParallel(int[,] matrix, int matrixSize, int[] linksSum)
        {
            Console.WriteLine("** SparseMatrixPageRankParallel caluculation **");
            Console.WriteLine();

            #region SparseMatrix

            Console.WriteLine("** SparseMatrix building **");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var nnz = new List<int>();

            var nnzCount = 0;
            var nnzCountList = new List<int> { nnzCount };

            var nnzPosition = new List<int>();

            for (int i = 0; i < _matrixSize; i++)
            {
                var localNnzCount = 0;

                for (int j = 0; j < _matrixSize; j++)
                {
                    if (matrix[i, j] != 0)
                    {
                        localNnzCount++;
                        nnz.Add(matrix[i, j]);
                        nnzPosition.Add(j);
                    }
                }

                nnzCount += localNnzCount;
                nnzCountList.Add(nnzCount);
            }

            stopwatch.Stop();

            Console.WriteLine($"Built completed, elapsed: {stopwatch.Elapsed.TotalMilliseconds}(ms)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            #endregion

            #region PageRank

            Console.WriteLine("** PageRank caluculation **");

            stopwatch = new Stopwatch();
            stopwatch.Start();

            var pagesRankOld = new double[matrixSize];
            var pagesRank = new double[matrixSize];

            for (int i = 0; i < matrixSize; i++)
                pagesRankOld[i] = 1.0D / matrixSize;

            for (int i = 0; i < 30; i++)
            {
                var old = pagesRankOld;

                Parallel.For(0, _matrixSize, y =>
                {
                    pagesRank[y] = 1 - 0.85;

                    var sum = 0.0D;

                    for (int h = 0; h < matrixSize; h++)
                    {
                        if (GetSparseMatrixValue(y, h, nnz, nnzCountList, nnzPosition) > -1 && linksSum[h] != 0)
                        {
                            sum += old[h] / linksSum[h];
                        }
                    }

                    sum = 0.85 * sum;
                    pagesRank[y] += sum;
                });

                pagesRankOld = pagesRank;
            }

            stopwatch.Stop();

            Console.WriteLine($"Calculated, elapsed: {stopwatch.Elapsed.TotalMilliseconds}(ms)");
            Console.WriteLine("** Done **");
            Console.WriteLine();

            #endregion

            Console.WriteLine("** Done SparseMatrixPageRank caluculation **");
            Console.WriteLine();
        }

        // Sparse matrix get value method
      
        public int GetSparseMatrixValue(int i, int j, List<int> nnz, List<int> nnzCountList, List<int> nnzPosition)
        {
            return nnzPosition
                .Skip(nnzCountList[i])
                .Take(nnzCountList[i + 1] - nnzCountList[i])
                .ToList()
                .IndexOf(j);
        }
    }
}
